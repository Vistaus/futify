package server

import (
	"context"
	"fmt"
	"log"

	"github.com/zmb3/spotify/v2"
)

func (s *Session) LoadSpecialPlaylists() string {
	uuid := "SpecialPlaylists"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	uuid1 := s.loadUserTracks(true)
	uuid2 := s.loadRecentlyPlay()
	uuid3 := s.loadTopTracks(true)
	playlists := []*Playlist{
		s.playlist[uuid1],
		s.playlist[uuid2],
		s.playlist[uuid3],
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Special Playlist", len(playlists), playlists)
	return uuid
}

func (s *Session) loadRecentlyPlay() string {
	uuid := "RecentlyPlay"
	if s.GetPlaylist(uuid) != nil {
		// already loaded don't redwnload it
		fmt.Println("Found uuid return " + uuid)
		return uuid
	}
	items, err := s.spotAPIClient.PlayerRecentlyPlayed(context.Background())

	if err != nil {
		log.Fatalf("couldn't get PlayerRecentlyPlayed: %v", err)
	}

	ids := []spotify.ID{}

	for _, track := range items {
		// Avoid storing the same track several times
		alreadyStored := false
		for _, trackid := range ids {
			if track.Track.ID == trackid {
				alreadyStored = true
				break
			}
		}

		if !alreadyStored {
			ids = append(ids, track.Track.ID)
		}
	}
	tracks, err := s.spotAPIClient.GetTracks(context.Background(), ids)
	myTracks := make([]*Track, len(tracks))
	image := ""
	for index, t := range tracks {
		myTracks[index] = NewTrack(t, s)
		if image == "" {
			image = myTracks[index].Image
		}
	}
	s.playlist[uuid] = NewPlaylist(uuid, "Recently Play", s.Username, image, 1, uint(len(items)), myTracks, playlistKindPlaylist, playlistFollowImpossible)
	fmt.Println(fmt.Sprintf("Found %d songs added in %s", len(myTracks), uuid))
	return uuid
}

func (s *Session) loadTopTracks(onlyFirstTracks bool) string {
	uuid := TopTracksPlaylistUUID
	if s.GetPlaylist(uuid) != nil && !onlyFirstTracks && s.GetPlaylist(uuid).TotalSize == len(s.GetPlaylist(uuid).tracks) {
		// already loaded don't redwnload it
		return uuid
	}
	items, err := s.spotAPIClient.CurrentUsersTopTracks(context.Background(), spotify.Limit(50))

	myTracks := make([]*Track, len(items.Tracks))
	if !onlyFirstTracks {
		myTracks = make([]*Track, items.Total)
	}
	image := ""
	stop := false
	page := 0

	for ; err == nil && !stop; err = s.spotAPIClient.NextPage(context.Background(), items) {
		for i, t := range items.Tracks {
			myTracks[page+i] = NewTrack(&t, s)
			if image == "" {
				image = myTracks[page+i].Image
			}
		}
		stop = onlyFirstTracks
		page += len(items.Tracks)
	}
	fmt.Println(fmt.Sprintf("Finish with %d on %d", page, items.Total))

	if err != nil && err != spotify.ErrNoMorePages {
		fmt.Println(fmt.Sprintf("Error to get top tracks %s", err.Error()))
	}

	fmt.Println(fmt.Sprintf("Add playlist Top Tracks with %d/%d tracks", len(myTracks), items.Total))
	s.playlist[uuid] = NewPlaylist(uuid, "Top Tracks", s.Username, image, 1, uint(items.Total), myTracks, playlistKindPlaylist, playlistFollowImpossible)
	return uuid
}

func (s *Session) loadUserTracks(onlyFirstTracks bool) string {
	uuid := UserTracksUUID
	if s.GetPlaylist(uuid) != nil && !onlyFirstTracks && s.GetPlaylist(uuid).TotalSize == len(s.GetPlaylist(uuid).tracks) {
		// already loaded don't redwnload it
		return uuid
	}
	items, err := s.spotAPIClient.CurrentUsersTracks(context.Background(), spotify.Limit(50))

	myTracks := make([]*Track, len(items.Tracks))
	if !onlyFirstTracks {
		myTracks = make([]*Track, items.Total)
	}
	image := ""
	stop := false
	page := 0

	for ; err == nil && !stop; err = s.spotAPIClient.NextPage(context.Background(), items) {
		for i, t := range items.Tracks {
			myTracks[page+i] = NewTrackSaved(t, s)
			if image == "" {
				image = myTracks[page+i].Image
			}
		}
		stop = onlyFirstTracks
		page += len(items.Tracks)
	}

	if err != nil && err != spotify.ErrNoMorePages {
		fmt.Println(fmt.Sprintf("Error to get user tracks %s", err.Error()))
	}

	fmt.Println(fmt.Sprintf("Add playlist Loved Tracks with %d/%d tracks", len(myTracks), items.Total))
	s.playlist[uuid] = NewPlaylist(uuid, "Loved Tracks", s.Username, image, 1, uint(items.Total), myTracks, playlistKindPlaylist, playlistFollowImpossible)
	return uuid
}

func (s *Session) LoadFeaturedPlaylists() string {
	uuid := "FeaturedPlaylists"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	m, p, _ := s.spotAPIClient.FeaturedPlaylists(context.Background())
	fmt.Println("Message from spotify=", m)
	playlist := make([]*Playlist, len(p.Playlists))
	for index, t := range p.Playlists {
		playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.Owner.DisplayName, SelectImageURL(t.Images), 0, t.Tracks.Total, []*Track{}, playlistKindPlaylist, playlistFollowFalse)
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Featured Playlist", p.Total, playlist)
	return uuid
}

func (s *Session) LoadUserPlaylists() string {
	uuid := "UserPlaylists"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	p, err := s.spotAPIClient.CurrentUsersPlaylists(context.Background())
	if err != nil {
		log.Fatalf("couldn't get UserTracks: %v", err)
	}
	playlist := make([]*Playlist, len(p.Playlists))
	for index, t := range p.Playlists {
		playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.Owner.DisplayName, SelectImageURL(t.Images), 0, t.Tracks.Total, []*Track{}, playlistKindPlaylist, playlistFollowTrue)
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Your playlists", p.Total, playlist)
	return uuid
}

func (s *Session) LoadMore(uuid string) string {
	playlists := s.GetPlaylists(uuid)
	if playlists == nil {
		fmt.Println(fmt.Printf("Please load playlist %s before call LoadMorelaylists", uuid))
		return uuid
	}
	if playlists.Size >= playlists.TotalSize {
		fmt.Println("Full load no need to load more")
		return uuid
	}

	if uuid == s.LoadUserPlaylists() {
		fmt.Println("Load more start")
		p, err := s.spotAPIClient.CurrentUsersPlaylists(context.Background(), spotify.Offset(playlists.Size))
		if err != nil {
			log.Fatalf("couldn't get UserTracks: %v", err)
		}
		playlist := make([]*Playlist, len(p.Playlists))
		fmt.Println(fmt.Sprintf("len res %d", len(p.Playlists)))
		for index, t := range p.Playlists {
			fmt.Println("append playlist")
			playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.Owner.DisplayName, SelectImageURL(t.Images), 0, t.Tracks.Total, []*Track{}, playlistKindPlaylist, playlistFollowTrue)
		}
		s.playlists[uuid] = NewPlaylists(uuid, playlists.Name, playlists.TotalSize, append(s.playlists[uuid].playlists, playlist...))
		fmt.Println(fmt.Sprintf("len playlists %d", len(s.playlists[uuid].playlists)))
		return s.LoadMore(uuid)
	} else if uuid == s.LoadFeaturedPlaylists() {
		_, p, _ := s.spotAPIClient.FeaturedPlaylists(context.Background(), spotify.Offset(playlists.Size))
		playlist := make([]*Playlist, len(p.Playlists))
		for index, t := range p.Playlists {
			playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.Owner.DisplayName, SelectImageURL(t.Images), 0, t.Tracks.Total, []*Track{}, playlistKindPlaylist, playlistFollowFalse)
		}
		s.playlists[uuid] = NewPlaylists(uuid, playlists.Name, playlists.TotalSize, append(s.playlists[uuid].playlists, playlist...))
		return s.LoadMore(uuid)
	} else if uuid == s.LoadUserAlbums() {
		p, err := s.spotAPIClient.CurrentUsersAlbums(context.Background(), spotify.Offset(playlists.Size))
		if err != nil {
			log.Fatalf("couldn't get UserTracks: %v", err)
		}
		playlist := make([]*Playlist, len(p.Albums))
		for index, t := range p.Albums {
			playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.AlbumGroup, SelectImageURL(t.Images), 0, uint(t.Tracks.Total), []*Track{}, playlistKindAlbum, playlistFollowTrue)
		}
		s.playlists[uuid] = NewPlaylists(uuid, playlists.Name, playlists.TotalSize, append(s.playlists[uuid].playlists, playlist...))
	} else if uuid == s.LoadUserShows() {
		p, err := s.spotAPIClient.CurrentUsersShows(context.Background(), spotify.Offset(playlists.Size))
		if err != nil {
			log.Fatalf("couldn't get UserShows: %v", err)
		}
		show := make([]*Playlist, len(p.Shows))
		for index, t := range p.Shows {
			show[index] = NewPlaylist(t.ID.String(), t.Name, t.Publisher, SelectImageURL(t.Images), 0, uint(t.Episodes.Total), []*Track{}, playlistKindShow, playlistFollowTrue)
		}
		s.playlists[uuid] = NewPlaylists(uuid, playlists.Name, playlists.TotalSize, append(s.playlists[uuid].playlists, show...))
	}
	return uuid
}

func (s *Session) LoadUserAlbums() string {
	uuid := "UserAlbums"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	p, err := s.spotAPIClient.CurrentUsersAlbums(context.Background())
	if err != nil {
		log.Fatalf("couldn't get UserTracks: %v", err)
	}
	playlist := make([]*Playlist, len(p.Albums))
	for index, t := range p.Albums {
		playlist[index] = NewPlaylist(t.ID.String(), t.Name, t.AlbumGroup, SelectImageURL(t.Images), 0, uint(t.Tracks.Total), []*Track{}, playlistKindAlbum, playlistFollowTrue)
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Your albums", p.Total, playlist)
	return uuid
}

func (s *Session) LoadUserShows() string {
	uuid := "UserShows"
	if s.GetPlaylists(uuid) != nil {
		// already loaded don't redwnload it
		return uuid
	}
	p, err := s.spotAPIClient.CurrentUsersShows(context.Background())
	if err != nil {
		log.Fatalf("couldn't get UserShows: %v", err)
	}
	show := make([]*Playlist, len(p.Shows))
	for index, t := range p.Shows {
		show[index] = NewPlaylist(t.ID.String(), t.Name, t.Publisher, SelectImageURL(t.Images), 0, uint(t.Episodes.Total), []*Track{}, playlistKindShow, playlistFollowTrue)
	}
	s.playlists[uuid] = NewPlaylists(uuid, "Your shows", p.Total, show)
	return uuid
}
