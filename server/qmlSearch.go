package server

import (
	"context"
	"fmt"

	"github.com/zmb3/spotify/v2"
)

func (s *Session) Search(search string) *SearchResults {
	res, err := s.spotAPIClient.Search(context.Background(), search, spotify.SearchTypeAlbum|spotify.SearchTypeArtist|spotify.SearchTypeTrack|spotify.SearchTypePlaylist|spotify.SearchTypeEpisode|spotify.SearchTypeShow)
	if err != nil {
		fmt.Println(err)
		return nil
	}
	s.searchResults = NewSearchResults(res, s)
	fmt.Println(fmt.Sprintf("Found results for %s", search))
	return s.searchResults
}
