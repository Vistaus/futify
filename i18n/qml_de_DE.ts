<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="29"/>
        <source>App:</source>
        <translation>App:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="36"/>
        <source>Futify is an unofficial native spotify client.</source>
        <translation>Futify ist ein inoffizieller nativer Client für Spotify</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="48"/>
        <source>issues</source>
        <translation>Probleme</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="61"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="94"/>
        <source>Contributors:</source>
        <translation>Mitwirkende</translation>
    </message>
</context>
<context>
    <name>AlbumView</name>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="89"/>
        <source>Remove from my library</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="89"/>
        <source>Add to my library</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Albums</name>
    <message>
        <location filename="../qml/components/Albums.qml" line="66"/>
        <source>Play</source>
        <translation>Abspielen</translation>
    </message>
</context>
<context>
    <name>Artists</name>
    <message>
        <location filename="../qml/components/Artists.qml" line="58"/>
        <source>popularity</source>
        <translation>Bekanntheit</translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../qml/pages/Home.qml" line="85"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="100"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="105"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="134"/>
        <source>Your playlists</source>
        <translation>Deine Playlists</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="164"/>
        <source>Your albums</source>
        <translation>Deine Alben</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="194"/>
        <source>Your shows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="224"/>
        <source>Your tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recently Play</source>
        <translation type="vanished">Zuletzt gehört</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="254"/>
        <source>Featured playlist</source>
        <translation>Vorgeschlagene Playlists</translation>
    </message>
    <message>
        <source>Top tracks</source>
        <translation type="vanished">Top Tracks</translation>
    </message>
</context>
<context>
    <name>HorizontalList</name>
    <message>
        <location filename="../qml/components/HorizontalList.qml" line="44"/>
        <source>Show more...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/pages/Login.qml" line="34"/>
        <source>Username</source>
        <translation>Nutzername</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="40"/>
        <source>Password</source>
        <translation>Passwort</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="53"/>
        <source>Log in</source>
        <translation>Anmelden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="67"/>
        <source>spotify.com</source>
        <translation>spotify.com</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="190"/>
        <source>Followed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="197"/>
        <source>Unfollowed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="218"/>
        <location filename="../qml/Main.qml" line="246"/>
        <source>Added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="225"/>
        <location filename="../qml/Main.qml" line="253"/>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlayerView</name>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="75"/>
        <source>No song</source>
        <translation>Kein Titel</translation>
    </message>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="75"/>
        <source>Buffering</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistView</name>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="84"/>
        <source>followers</source>
        <translation>Follower</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="85"/>
        <source>tracks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="89"/>
        <source>Unfollow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="89"/>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Playlists</name>
    <message>
        <source>Play</source>
        <translation type="vanished">Abspielen</translation>
    </message>
</context>
<context>
    <name>PlaylistsType</name>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="15"/>
        <source>Your playlists</source>
        <translation type="unfinished">Deine Playlists</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="17"/>
        <location filename="../qml/model/PlaylistsType.qml" line="23"/>
        <source>Your albums</source>
        <translation type="unfinished">Deine Alben</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="19"/>
        <source>Featured playlist</source>
        <translation type="unfinished">Vorgeschlagene Playlists</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="21"/>
        <source>Your tracks</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PlaylistsView</name>
    <message>
        <location filename="../qml/components/PlaylistsView.qml" line="66"/>
        <source>Play</source>
        <translation type="unfinished">Abspielen</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="30"/>
        <source>Queue</source>
        <translation>Wiedergabeliste</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="34"/>
        <source>Next</source>
        <translation>Nächstes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="42"/>
        <source>Play/Pause</source>
        <translation>Abspielen/Pause</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="54"/>
        <source>Precedent</source>
        <translation>Vorhergehendes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="62"/>
        <source>Clear</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="71"/>
        <source>Repeat</source>
        <translation>Wiederholen</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/pages/Search.qml" line="41"/>
        <source>Search</source>
        <translation>Suchen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="57"/>
        <source>Tracks</source>
        <translation>Tracks</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="68"/>
        <source>Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="79"/>
        <source>Albums</source>
        <translation>Alben</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="90"/>
        <source>Playlists</source>
        <translation>Playlisten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="101"/>
        <source>Shows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="112"/>
        <source>Artists</source>
        <translation>Künstler</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="27"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="34"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="43"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>Theme:</source>
        <translation>Erscheinungsbild</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="145"/>
        <source>Lang:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>German</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Spanish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>French</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Hungarian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Dutch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="202"/>
        <source>Quality:</source>
        <translation>Qualität</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>low: prefer data over quality</source>
        <translation>niedrig: datensparend, bei geringer Qualität</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>medium</source>
        <translation>mittel</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>high: prefer quality over data</source>
        <translation>hoch: beste Qualität, hoher Datenverbrauch</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="259"/>
        <source>Error song:</source>
        <translation>Error song:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>silence: 3 seconds</source>
        <translation>Stille: 3 Sekunden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>error: usefull to know what happens</source>
        <translation>Fehler: Gut zu wissen, was passiert.</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="315"/>
        <source>Account:</source>
        <translation>Account:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="326"/>
        <source>Log out</source>
        <translation>Abmelden</translation>
    </message>
</context>
<context>
    <name>ShowView</name>
    <message>
        <location filename="../qml/components/ShowView.qml" line="91"/>
        <source>Unfollow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/components/ShowView.qml" line="91"/>
        <source>Follow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Shows</name>
    <message>
        <location filename="../qml/components/Shows.qml" line="66"/>
        <source>Play</source>
        <translation type="unfinished">Abspielen</translation>
    </message>
</context>
<context>
    <name>TrackListItem</name>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="55"/>
        <source>popularity</source>
        <translation>Bekanntheit</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="63"/>
        <source>Delete</source>
        <translation>Löschen</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="74"/>
        <source>Play</source>
        <translation>Abspielen</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="82"/>
        <source>End Queue</source>
        <translation>Abspielliste beenden</translation>
    </message>
</context>
<context>
    <name>UserMetricsHelper</name>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="15"/>
        <source>tracks played today</source>
        <translation>Tracks heute abgespielt</translation>
    </message>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="16"/>
        <source>No tracks played today</source>
        <translation>Heute keinen Track abgespielt</translation>
    </message>
</context>
</TS>
