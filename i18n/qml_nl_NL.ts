<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl_NL">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="29"/>
        <source>App:</source>
        <translation>App:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="36"/>
        <source>Futify is an unofficial native spotify client.</source>
        <translation>Futify is een onofficiële Spotify-app voor Ubuntu Touch.</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="48"/>
        <source>issues</source>
        <translation>Problemen</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="61"/>
        <source>Author:</source>
        <translation>Maker:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="94"/>
        <source>Contributors:</source>
        <translation>Bijdragers:</translation>
    </message>
</context>
<context>
    <name>AlbumView</name>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="89"/>
        <source>Remove from my library</source>
        <translation>Verwijderen uit verzameling</translation>
    </message>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="89"/>
        <source>Add to my library</source>
        <translation>Toevoegen aan verzameling</translation>
    </message>
</context>
<context>
    <name>Albums</name>
    <message>
        <location filename="../qml/components/Albums.qml" line="66"/>
        <source>Play</source>
        <translation>Afspelen</translation>
    </message>
</context>
<context>
    <name>Artists</name>
    <message>
        <location filename="../qml/components/Artists.qml" line="58"/>
        <source>popularity</source>
        <translation>Populariteit</translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../qml/pages/Home.qml" line="85"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="100"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="105"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="134"/>
        <source>Your playlists</source>
        <translation>Mijn afspeellijsten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="164"/>
        <source>Your albums</source>
        <translation>Mijn albums</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="194"/>
        <source>Your shows</source>
        <translation>Mijn shows</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="224"/>
        <source>Your tracks</source>
        <translation>Mijn nummers</translation>
    </message>
    <message>
        <source>Recently Play</source>
        <translation type="vanished">Onlangs afgespeeld</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="254"/>
        <source>Featured playlist</source>
        <translation>Uitgelichte afspeellijst</translation>
    </message>
    <message>
        <source>Top tracks</source>
        <translation type="vanished">Topnummers</translation>
    </message>
</context>
<context>
    <name>HorizontalList</name>
    <message>
        <location filename="../qml/components/HorizontalList.qml" line="44"/>
        <source>Show more...</source>
        <translation>Meer tonen…</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/pages/Login.qml" line="34"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="40"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="53"/>
        <source>Log in</source>
        <translation>Inloggen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="67"/>
        <source>spotify.com</source>
        <translation>spotify.com</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="190"/>
        <source>Followed</source>
        <translation>Gevolgd</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="197"/>
        <source>Unfollowed</source>
        <translation>Ontvolgd</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="218"/>
        <location filename="../qml/Main.qml" line="246"/>
        <source>Added</source>
        <translation>Toegevoegd</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="225"/>
        <location filename="../qml/Main.qml" line="253"/>
        <source>Removed</source>
        <translation>Verwijderd</translation>
    </message>
</context>
<context>
    <name>PlayerView</name>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="75"/>
        <source>No song</source>
        <translation>Geen nummer</translation>
    </message>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="75"/>
        <source>Buffering</source>
        <translation>Bezig met inladen…</translation>
    </message>
</context>
<context>
    <name>PlaylistView</name>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="84"/>
        <source>followers</source>
        <translation>volgers</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="85"/>
        <source>tracks</source>
        <translation>nummers</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="89"/>
        <source>Unfollow</source>
        <translation>Ontvolgen</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="89"/>
        <source>Follow</source>
        <translation>Volgen</translation>
    </message>
</context>
<context>
    <name>Playlists</name>
    <message>
        <source>Play</source>
        <translation type="vanished">Afspelen</translation>
    </message>
</context>
<context>
    <name>PlaylistsType</name>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="15"/>
        <source>Your playlists</source>
        <translation>Mijn afspeellijsten</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="17"/>
        <location filename="../qml/model/PlaylistsType.qml" line="23"/>
        <source>Your albums</source>
        <translation>Mijn albums</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="19"/>
        <source>Featured playlist</source>
        <translation>Uitgelichte afspeellijst</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="21"/>
        <source>Your tracks</source>
        <translation>Mijn nummers</translation>
    </message>
</context>
<context>
    <name>PlaylistsView</name>
    <message>
        <location filename="../qml/components/PlaylistsView.qml" line="66"/>
        <source>Play</source>
        <translation>Afspelen</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="30"/>
        <source>Queue</source>
        <translation>Wachtrij</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="34"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="42"/>
        <source>Play/Pause</source>
        <translation>Afspelen/Pauzeren</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="54"/>
        <source>Precedent</source>
        <translation>Vorige</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="62"/>
        <source>Clear</source>
        <translation>Wissen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="71"/>
        <source>Repeat</source>
        <translation>Herhalen</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/pages/Search.qml" line="41"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="57"/>
        <source>Tracks</source>
        <translation>Nummers</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="68"/>
        <source>Episodes</source>
        <translation>Afleveringen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="79"/>
        <source>Albums</source>
        <translation>Albums</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="90"/>
        <source>Playlists</source>
        <translation>Afspeellijsten</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="101"/>
        <source>Shows</source>
        <translation>Shows</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="112"/>
        <source>Artists</source>
        <translation>Artiesten</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="27"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="34"/>
        <source>Reset</source>
        <translation>Standaardwaarden</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="43"/>
        <source>Save</source>
        <translation>Opslaan</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>Theme:</source>
        <translation>Thema:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Dark</source>
        <translation>Donker</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Light</source>
        <translation>Licht</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>System</source>
        <translation>Systeemthema</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="145"/>
        <source>Lang:</source>
        <translation>Taal:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>German</source>
        <translation>Duits</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Spanish</source>
        <translation>Spaans</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>French</source>
        <translation>Frans</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Hungarian</source>
        <translation>Hongaars</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Dutch</source>
        <translation>Nederlands</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Swedish</source>
        <translation>Zweeds</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>English</source>
        <translation>Engels</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="202"/>
        <source>Quality:</source>
        <translation>Kwaliteit:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>low: prefer data over quality</source>
        <translation>Laag: lage kwaliteit (bespaart mb&apos;s)</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>medium</source>
        <translation>Gemiddeld</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>high: prefer quality over data</source>
        <translation>Hoog: hoge kwaliteit (kost extra mb&apos;s)</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="259"/>
        <source>Error song:</source>
        <translation>Foutmelding:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>silence: 3 seconds</source>
        <translation>3 seconden verbergen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>error: usefull to know what happens</source>
        <translation>Foutmelding: nuttig om de oorzaak te achterhalen</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="315"/>
        <source>Account:</source>
        <translation>Account:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="326"/>
        <source>Log out</source>
        <translation>Uitloggen</translation>
    </message>
</context>
<context>
    <name>ShowView</name>
    <message>
        <location filename="../qml/components/ShowView.qml" line="91"/>
        <source>Unfollow</source>
        <translation>Ontvolgen</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowView.qml" line="91"/>
        <source>Follow</source>
        <translation>Volgen</translation>
    </message>
</context>
<context>
    <name>Shows</name>
    <message>
        <location filename="../qml/components/Shows.qml" line="66"/>
        <source>Play</source>
        <translation>Afspelen</translation>
    </message>
</context>
<context>
    <name>TrackListItem</name>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="55"/>
        <source>popularity</source>
        <translation>Populariteit</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="63"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="74"/>
        <source>Play</source>
        <translation>Afspelen</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="82"/>
        <source>End Queue</source>
        <translation>Einde van wachtrij</translation>
    </message>
</context>
<context>
    <name>UserMetricsHelper</name>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="15"/>
        <source>tracks played today</source>
        <translation>nummers afgespeeld (vandaag)</translation>
    </message>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="16"/>
        <source>No tracks played today</source>
        <translation>Je hebt vandaag niks afgespeeld</translation>
    </message>
</context>
</TS>
