<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_EN">
<context>
    <name>About</name>
    <message>
        <location filename="../qml/pages/About.qml" line="9"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="29"/>
        <source>App:</source>
        <translation>App:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="36"/>
        <source>Futify is an unofficial native spotify client.</source>
        <translation>Futify is an unofficial native spotify client.</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="48"/>
        <source>issues</source>
        <translation>issues</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="61"/>
        <source>Author:</source>
        <translation>Author:</translation>
    </message>
    <message>
        <location filename="../qml/pages/About.qml" line="94"/>
        <source>Contributors:</source>
        <translation>Contributors:</translation>
    </message>
</context>
<context>
    <name>AlbumView</name>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="89"/>
        <source>Remove from my library</source>
        <translation>Remove from my library</translation>
    </message>
    <message>
        <location filename="../qml/components/AlbumView.qml" line="89"/>
        <source>Add to my library</source>
        <translation>Add to my library</translation>
    </message>
</context>
<context>
    <name>Albums</name>
    <message>
        <location filename="../qml/components/Albums.qml" line="66"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
</context>
<context>
    <name>Artists</name>
    <message>
        <location filename="../qml/components/Artists.qml" line="58"/>
        <source>popularity</source>
        <translation>popularity</translation>
    </message>
</context>
<context>
    <name>Home</name>
    <message>
        <location filename="../qml/pages/Home.qml" line="85"/>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="100"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="105"/>
        <source>About</source>
        <translation>About</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="134"/>
        <source>Your playlists</source>
        <translation>Your playlists</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="164"/>
        <source>Your albums</source>
        <translation>Your albums</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="194"/>
        <source>Your shows</source>
        <translation>Your shows</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="224"/>
        <source>Your tracks</source>
        <translation>Your tracks</translation>
    </message>
    <message>
        <location filename="../qml/pages/Home.qml" line="254"/>
        <source>Featured playlist</source>
        <translation>Featured playlist</translation>
    </message>
</context>
<context>
    <name>HorizontalList</name>
    <message>
        <location filename="../qml/components/HorizontalList.qml" line="44"/>
        <source>Show more...</source>
        <translation>Show more...</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/pages/Login.qml" line="34"/>
        <source>Username</source>
        <translation>Username</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="40"/>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="53"/>
        <source>Log in</source>
        <translation>Log in</translation>
    </message>
    <message>
        <location filename="../qml/pages/Login.qml" line="67"/>
        <source>spotify.com</source>
        <translation>spotify.com</translation>
    </message>
</context>
<context>
    <name>Main</name>
    <message>
        <location filename="../qml/Main.qml" line="190"/>
        <source>Followed</source>
        <translation>Followed</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="197"/>
        <source>Unfollowed</source>
        <translation>Unfollowed</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="218"/>
        <location filename="../qml/Main.qml" line="246"/>
        <source>Added</source>
        <translation>Added</translation>
    </message>
    <message>
        <location filename="../qml/Main.qml" line="225"/>
        <location filename="../qml/Main.qml" line="253"/>
        <source>Removed</source>
        <translation>Removed</translation>
    </message>
</context>
<context>
    <name>PlayerView</name>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="75"/>
        <source>No song</source>
        <translation>No song</translation>
    </message>
    <message>
        <location filename="../qml/components/PlayerView.qml" line="75"/>
        <source>Buffering</source>
        <translation>Buffering</translation>
    </message>
</context>
<context>
    <name>PlaylistView</name>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="84"/>
        <source>followers</source>
        <translation>followers</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="85"/>
        <source>tracks</source>
        <translation>tracks</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="89"/>
        <source>Unfollow</source>
        <translation>Unfollow</translation>
    </message>
    <message>
        <location filename="../qml/components/PlaylistView.qml" line="89"/>
        <source>Follow</source>
        <translation>Follow</translation>
    </message>
</context>
<context>
    <name>PlaylistsType</name>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="15"/>
        <source>Your playlists</source>
        <translation>Your playlists</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="17"/>
        <location filename="../qml/model/PlaylistsType.qml" line="23"/>
        <source>Your albums</source>
        <translation>Your albums</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="19"/>
        <source>Featured playlist</source>
        <translation>Featured playlist</translation>
    </message>
    <message>
        <location filename="../qml/model/PlaylistsType.qml" line="21"/>
        <source>Your tracks</source>
        <translation>Your tracks</translation>
    </message>
</context>
<context>
    <name>PlaylistsView</name>
    <message>
        <location filename="../qml/components/PlaylistsView.qml" line="66"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
</context>
<context>
    <name>Queue</name>
    <message>
        <location filename="../qml/pages/Queue.qml" line="30"/>
        <source>Queue</source>
        <translation>Queue</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="34"/>
        <source>Next</source>
        <translation>Next</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="42"/>
        <source>Play/Pause</source>
        <translation>Play/Pause</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="54"/>
        <source>Precedent</source>
        <translation>Precedent</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="62"/>
        <source>Clear</source>
        <translation>Clear</translation>
    </message>
    <message>
        <location filename="../qml/pages/Queue.qml" line="71"/>
        <source>Repeat</source>
        <translation>Repeat</translation>
    </message>
</context>
<context>
    <name>Search</name>
    <message>
        <location filename="../qml/pages/Search.qml" line="41"/>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="57"/>
        <source>Tracks</source>
        <translation>Tracks</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="68"/>
        <source>Episodes</source>
        <translation>Episodes</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="79"/>
        <source>Albums</source>
        <translation>Albums</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="90"/>
        <source>Playlists</source>
        <translation>Playlists</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="101"/>
        <source>Shows</source>
        <translation>Shows</translation>
    </message>
    <message>
        <location filename="../qml/pages/Search.qml" line="112"/>
        <source>Artists</source>
        <translation>Artists</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../qml/pages/Settings.qml" line="27"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="34"/>
        <source>Reset</source>
        <translation>Reset</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="43"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="88"/>
        <source>Theme:</source>
        <translation>Theme:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Dark</source>
        <translation>Dark</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <source>Light</source>
        <translation>Light</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="97"/>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="145"/>
        <source>Lang:</source>
        <translation>Lang:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>German</source>
        <translation>German</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Spanish</source>
        <translation>Spanish</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>French</source>
        <translation>French</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Hungarian</source>
        <translation>Hungarian</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Dutch</source>
        <translation>Dutch</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>Swedish</source>
        <translation>Swedish</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="154"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="202"/>
        <source>Quality:</source>
        <translation>Quality:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>low: prefer data over quality</source>
        <translation>low: prefer data over quality</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>medium</source>
        <translation>medium</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="211"/>
        <source>high: prefer quality over data</source>
        <translation>high: prefer quality over data</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="259"/>
        <source>Error song:</source>
        <translation>Error song:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>silence: 3 seconds</source>
        <translation>silence: 3 seconds</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="268"/>
        <source>error: usefull to know what happens</source>
        <translation>error: usefull to know what happens</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="315"/>
        <source>Account:</source>
        <translation>Account:</translation>
    </message>
    <message>
        <location filename="../qml/pages/Settings.qml" line="326"/>
        <source>Log out</source>
        <translation>Log out</translation>
    </message>
</context>
<context>
    <name>ShowView</name>
    <message>
        <location filename="../qml/components/ShowView.qml" line="91"/>
        <source>Unfollow</source>
        <translation>Unfollow</translation>
    </message>
    <message>
        <location filename="../qml/components/ShowView.qml" line="91"/>
        <source>Follow</source>
        <translation>Follow</translation>
    </message>
</context>
<context>
    <name>Shows</name>
    <message>
        <location filename="../qml/components/Shows.qml" line="66"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
</context>
<context>
    <name>TrackListItem</name>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="55"/>
        <source>popularity</source>
        <translation>popularity</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="63"/>
        <source>Delete</source>
        <translation>Delete</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="74"/>
        <source>Play</source>
        <translation>Play</translation>
    </message>
    <message>
        <location filename="../qml/components/TrackListItem.qml" line="82"/>
        <source>End Queue</source>
        <translation>End Queue</translation>
    </message>
</context>
<context>
    <name>UserMetricsHelper</name>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="15"/>
        <source>tracks played today</source>
        <translation>tracks played today</translation>
    </message>
    <message>
        <location filename="../qml/services/UserMetricsHelper.qml" line="16"/>
        <source>No tracks played today</source>
        <translation>No tracks played today</translation>
    </message>
</context>
</TS>
