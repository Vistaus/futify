import QtQuick 2.12

QtObject {
    property string uuid: ""
    property string image: ""
    property string name: ""
    property string group: ""
    property int size: 0
    property string follow: ""

    function getTrack(index) {
        return spotSession.getTrackByAlbum(uuid, index);
    }
}