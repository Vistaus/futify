import QtQuick 2.12
import Lomiri.Components 1.3

import "../components"
import "../model"

Page {
    id: page

    property ShowType show

    signal startShow(ShowType show)
    signal startTrack(var track)
    signal addToEndQueue(var track)
    signal follow(ShowType show)
    signal unfollow(ShowType show)

    function refresh(showTypeFactory) {
        if (show.uuid != '') {
            show = showTypeFactory.createObject(page, spotSession.getShow(spotSession.loadShow(show.uuid)));
        }
    }

    header: PageHeader {
        id: header
        title: show.name
        StyleHints {
            dividerColor: LomiriColors.green
        }
    }

    ShowView {
        id: playlistView
        show: page.show
        anchors.top: header.bottom
        height: page.height - header.height
        width: page.width
        onStartShow: page.startShow(show)
        onStartTrack: page.startTrack(track)
        onAddToEndQueue: page.addToEndQueue(track)
        onFollow: page.follow(show)
        onUnfollow: page.unfollow(show)
    }
}