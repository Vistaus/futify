import QtQuick 2.12
import Lomiri.Components 1.3
import QtGraphicalEffects 1.0

ListItem {
    property var searchResult
    Row {
        height: parent.height
        width: parent.width
        padding: units.gu(2)
        spacing: units.gu(2)

        Item {
            id: itemImage
            width: units.gu(6)
            height: units.gu(6)
            anchors.verticalCenter: parent.verticalCenter
            Image {
                id: image
                source: searchResult.image
                width: parent.width
                height: parent.height
                anchors.verticalCenter: parent.verticalCenter
                fillMode: Image.PreserveAspectFit
                visible: false
            }
            OpacityMask {
                anchors.fill: image
                source: image
                width: image.width
                height: image.height
                maskSource: Rectangle {
                    width: image.width
                    height: image.height
                    radius: 5
                    visible: false // this also needs to be invisible or it will cover up the image
                }
            }
        }
        Column {
            anchors.verticalCenter: itemImage.verticalCenter
            width: parent.width - itemImage.width - parent.spacing - parent.padding
            Label { text: searchResult.name; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount: 1; textSize: Label.Large }
            Label { text: searchResult.owner; width: parent.width; elide: Text.ElideLeft; wrapMode: Text.Wrap; maximumLineCount:1; }
            Row {
                width: parent.width
                spacing: units.gu(1)
                Label { width: parent.width - parent.spacing; text: searchResult.popularity+'%'; textSize: Label.Small; horizontalAlignment: Text.AlignRight; }
            }
        }
    }
}