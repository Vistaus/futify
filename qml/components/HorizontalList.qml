import QtQuick 2.12
import QtQuick.Layouts 1.3
import Lomiri.Components 1.3
import QtGraphicalEffects 1.0

Column {
    spacing: units.gu(2)
    padding: units.gu(1)

    property string title: ''
    property string icon: ''
    property string uuid: ''
    property bool showInfo: false
    property var getSize//(uuid)
    property var getTotalSize//(uuid)
    property var getData//(uuid, index)

    signal start(var data)
    signal goToDetails(var data)
    signal showMore(var uuid)

    RowLayout {
        spacing: units.gu(1)
        width: parent.width - units.gu(3)

        Icon {
            visible: icon !== ''
            height: titleLabel.height
            name: icon
        }
        Label {
            id: titleLabel
            text: title
            textSize: Label.Large
        }
        Label {
            text: uuid == '' ? '' : ' ' + getSize(uuid) + '/' + getTotalSize(uuid)
            textSize: Label.Small
        }
        Item {
            Layout.fillWidth: true
        }
        LightButton {
            text: qsTr("Show more...")
            onClicked: {
                showMore(uuid)
            }
        }
    }
    ListView {
        width: parent.width - units.gu(4)
        height: units.gu(16)
        orientation: ListView.Horizontal
        model: uuid !== '' ? getSize(uuid) : 0
        spacing: units.gu(2)
        delegate: ListItem {
            width: units.gu(16)
            height: parent.height
            divider.visible: false

            // IMAGE
            Item {
                width: parent.width
                height: parent.height
                anchors.verticalCenter: parent.verticalCenter
                Image {
                    id: image
                    source: getData(uuid, index).image
                    width: parent.width
                    height: parent.height
                    anchors.verticalCenter: parent.verticalCenter
                    fillMode: Image.PreserveAspectFit
                    visible: false
                }
                OpacityMask {
                    anchors.fill: image
                    source: image
                    width: image.width
                    height: image.height
                    maskSource: Rectangle {
                        width: image.width
                        height: image.height
                        radius: 5
                        visible: false // this also needs to be invisible or it will cover up the image
                    }
                }
            }

            // PLAY
            Rectangle {
                color: "#99FFFFFF"
                width: units.gu(5)
                height: units.gu(5)
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                radius: 50
            }
            Item {
                width: parent.width
                height: parent.height - infoRectangle.height
                anchors.top: infoRectangle.bottom
                TapHandler {
                    onTapped: start(getData(uuid, index))
                }
            }
            Icon {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                name: 'media-preview-start'
                color: "#000000"
                width: units.gu(5)
                height: units.gu(5)
            }

            // INFOS
            Rectangle {
                id: infoRectangle
                visible: showInfo
                color: "#95000000"
                width: units.gu(3)
                height: units.gu(3)
                radius: 50
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: units.gu(1)
            }
            Item {
                visible: showInfo
                width: units.gu(5)
                height: units.gu(5)
                anchors.top: parent.top
                anchors.right: parent.right
                TapHandler {
                    onTapped: goToDetails(getData(uuid, index))
                }
            }
            Icon {
                visible: showInfo
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: units.gu(1)
                name: 'info'
                color: "#ffffff"
                width: units.gu(3)
                height: units.gu(3)
            }

            // LABEL
            Rectangle {
                color: "#95000000"
                width: parent.width
                height: label.height
                anchors.bottom: parent.bottom
            }
            Label {
                id: label
                color: "#ffffff"
                text: getData(uuid, index).name
                anchors.bottom: parent.bottom
                width: parent.width
                elide: Text.ElideLeft
                wrapMode: Text.Wrap
                maximumLineCount: 1
                textSize: Label.Large
                horizontalAlignment: Text.AlignHCenter
            }
        }
    }
}